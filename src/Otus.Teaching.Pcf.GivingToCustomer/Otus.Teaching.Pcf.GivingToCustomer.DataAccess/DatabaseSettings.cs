﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class DatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
