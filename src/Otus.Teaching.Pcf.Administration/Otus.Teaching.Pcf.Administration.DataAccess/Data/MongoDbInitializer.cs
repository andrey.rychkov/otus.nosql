﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;

using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoDbInitializer: IDbInitializer
    {
        private readonly MongoDataContext _dataContext;

        public MongoDbInitializer(MongoDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.DropDatabase();
            _dataContext.Roles.DeleteMany(r => true);
            _dataContext.Employee.DeleteMany(r => true);

            _dataContext.Roles.InsertMany(FakeDataFactory.Roles);
            _dataContext.Employee.InsertMany(FakeDataFactory.Employees);
        }
    }
}
